var app = angular.module('app', []);

app.controller('listController', function ($scope) {
	$scope.list = [];

	$scope.addItem = function(){
		console.log("Item: " + $scope.newItem);
		$scope.list.push({
			'text': $scope.newItem
			, 'done': false
		});
		$scope.newItem = "";
	}
});